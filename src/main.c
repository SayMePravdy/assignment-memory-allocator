#include "test.h"

int main() {
	if (run_tests()) {
		printf("Success");
		return 0;
	}
	printf("Tests failed");
	return 1;
}