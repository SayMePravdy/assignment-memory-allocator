#define _DEFAULT_SOURCE
#include "test.h"
#include <unistd.h>

#define HEAP_SIZE 10000

void* heap;
struct block_header* start_block;

bool test_heap_init() {
	printf("TEST INIT STARTED\n");
	heap = heap_init(HEAP_SIZE);
	if (heap == NULL) {
		printf("Initialization failed\nTEST INIT FAILED\n");
		return false;
	}
	printf("TEST INIT PASSED\n");
	return true;
}

bool test1() {
	printf("TEST 1 STARTED\n");
	size_t query = 500;
	void* data = _malloc(query);
	if (data == NULL) {
		printf("Error! NULL\n");
		return false;
	}
	debug_heap(stdout, start_block);
	if (start_block->is_free != false || start_block->capacity.bytes != query) {
		printf("TEST 1 FAILED\n\n");
		return false;
	}
	
	_free(data);
	printf("TEST 1 PASSED\n\n");
	return true;
}

bool test2() {
	printf("TEST 2 STARTED\n");
	size_t query = 500;
	void* data1 = _malloc(query);
	void* data2 = _malloc(query);
	debug_heap(stdout, start_block);
	_free(data1);
	debug_heap(stdout, start_block);
	struct block_header *data1_block = block_get_header(data1);
    struct block_header *data2_block = block_get_header(data2);
	if (data1_block->is_free == false || data2_block->is_free == true) {
		printf("TEST 2 FAILED\n\n");
		return false;
	}
	_free(data2);
	_free(data1);
	printf("TEST 2 PASSED\n\n");
	return true;
}

bool test3() {
	printf("TEST 3 STARTED\n");
	size_t query = 500;
	void* data1 = _malloc(query);
	void* data2 = _malloc(query);
	void* data3 = _malloc(query);
	debug_heap(stdout, start_block);
	_free(data1);
	_free(data2);
	debug_heap(stdout, start_block);
	struct block_header *data1_block = block_get_header(data1);
    struct block_header *data2_block = block_get_header(data2);
    struct block_header *data3_block = block_get_header(data3);
	if (data1_block->is_free == false || data2_block->is_free == false || data3_block->is_free == true) {
		printf("TEST 3 FAILED\n\n");
		return false;
	}
	_free(data3);
	_free(data1);
	printf("TEST 3 PASSED\n\n");
	return true;
}

bool test4() {
	printf("TEST 4 STARTED\n");
	void* data1 = _malloc(HEAP_SIZE);
	void* data2 = _malloc(HEAP_SIZE);
	debug_heap(stdout, start_block);
	int cnt = 0;
	struct block_header* block = block_get_header(data1);
	while(block->next != NULL) {
		cnt++;
		block = block->next;
	}
	if (cnt != 2) {
		printf("TEST 4 FAILED\n\n");
		return false;
	}
	_free(data1);
	_free(data2);
	_free(data1);
	printf("TEST 4 PASSED\n\n");
	return true;
}

bool test5() {
	printf("TEST 5 STARTED\n");
	struct block_header* last_block = start_block;
	debug_heap(stdout, start_block);
	while(last_block->next != NULL) {
    	last_block = last_block->next;
	}
	debug_heap(stdout, start_block);
	map_pages((uint8_t *) last_block + size_from_capacity(last_block->capacity).bytes, 1000, MAP_FIXED);
	debug_heap(stdout, start_block);
    void* data = _malloc(5 * HEAP_SIZE);
	debug_heap(stdout, start_block);
	struct block_header* data_block = block_get_header(data);
	if (data_block == last_block) {
		printf("TEST 5 FAILED\n");
		return false;
	}
    _free(data);
	debug_heap(stdout, start_block);
	printf("TEST 5 PASSED\n\n");
	return true;
}

bool run_tests() {
	if (test_heap_init()) {
		start_block = (struct block_header*) heap;
		bool check = test1() && test2() && test3() && test4() && test5();
		return check;
	}
	return false;
	
}